#!/usr/bin/env python
import ics
import argparse
from datetime import datetime


def readFile(fileName):
    f = open(fileName)

    c = ics.Calendar(f.read())

    f.close()

    weekDict = {}
    todayIso = datetime.today().isocalendar()
    worktimeToday = 0

    for event in c.events:
        isoDay = event.begin.datetime.isocalendar()
        if isoDay == todayIso:
            worktimeToday += getWorkTime(event)

        weekString = '{}-{:02}'.format(isoDay[0], isoDay[1])

        if weekString not in weekDict:
            weekDict[weekString] = []

        weekDict[weekString].append(event)

    for week, events in sorted(weekDict.items()):
        totalTime = sum([getWorkTime(event) for event in events])
        print('{}: {}'.format(week, totalTime / 3600))

    print()
    print('Today: {}'.format(worktimeToday / 3600))


def getWorkTime(event):
    if event.all_day:
        return 5 * 3600

    duration = event.duration.seconds

    if 'L' in event.name:
        return duration - 1800
    return duration


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file")

    args = parser.parse_args()

    readFile(args.file)


if __name__ == "__main__":
    main()
